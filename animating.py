from Tkinter import *
from os import remove
import wand.image as wi
from wand.sequence import *

class Application(Frame): 
    def __init__(self,master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()
        
    def callback(self):
        sprites = self.spriteSheet.get()
        f = int(self.frames.get())
        w = int(self.width.get())
        h = int(self.height.get())
        xStart = int(self.startx.get())
        xPos = xStart
        yPos = int(self.y.get())
        name = self.gifName.get()
        self.cutsheet(sprites, f, w, h, cPos, xStart, 25)
        #self.cutSheet(sprites, f, w, h, xPos, xStart, yPos)
        self.animate(f, name)
        for frame in range(0,f):
            fileName = str(frame) + ".gif"
            remove(fileName)
    
    ## Split the image up into serveral images labelled numerically and put into a
    ## single directory.

    def cutSheet(self, sprites, f, w, h, curx, sx, y):
        for frame in range(0,f):
            name = str(frame) + ".gif"
            with wi.Image(filename=sprites) as img:
                with img.convert("gif") as convertedImg:
                    convertedImg.crop(curx,y,width = w,height = h)
                    convertedImg.save(filename=name)
        currentx = w * frame + sx

    ## Animate the Images int he Directory

    def animate(self, frames, gifName):
        with wi.Image(filename="0.gif") as img:
            for frame in range(1,frames):
                current = str(frame) + ".gif"
                img.sequence.append(wi.Image(filename=current))
                img.sequence.append(img)
            img.save(filename=gifName)
 
    ## Remove the Animation Files
 
    def removeFiles(self, frames):
        for frame in range(0,frames):
            fileName = str(frame) + ".gif"
            remove(fileName)
 
    def createWidgets(self):
        self.createLabels()
        self.createEntries()
        self.enterButtons = Button(self, text="Go", command=self.callback)
        self.enterButtons.grid(column=2)
        
    def createEntries(self):
        self.spriteSheet = Entry(self)
        self.spriteSheet.grid(column=2,row=0)
        self.gifName = Entry(self)
        self.gifName.grid(column=2, row=1)
        self.frames = Entry(self)
        self.frames.grid(column=2,row=2)
        self.width = Entry(self)
        self.width.grid(column=2,row=3)
        self.height = Entry(self)
        self.height.grid(column=2,row=4)
        self.startx = Entry(self)
        self.startx.grid(column=2,row=5)
        self.y = Entry(self)
        self.y.grid(column=2,row=6)
        
    def createLabels(self):
        Label(self, text="Where is the sprite sheet located?").grid()
        Label(self, text="What will the end gif be called?").grid()
        Label(self, text="How many frames does this majestic shit have?").grid()
        Label(self, text="The width?").grid()
        Label(self, text="The height?").grid()
        Label(self, text="The starting x value?").grid()
        Label(self, text="The starting y value?").grid()
     
app = Application()
app.master.title('Animation Centre')
app.mainloop()
